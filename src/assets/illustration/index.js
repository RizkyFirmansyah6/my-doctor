import ILLogo from './logo.svg';
import ILGetStarted from './get-started.png';
import ILNullPhoto from './null-photo.png';
import ILCatUmum from './ic-dok-umum.svg';
import ILCatPsikiater from './ic-dok-psikiater.svg';
import ILCatObat from './ic-dok-obat.svg';
import ILCatAnak from './ic-dok-anak.svg';
import ILHospitalBG from './hospitals-background.png';

export {
  ILLogo,
  ILGetStarted,
  ILNullPhoto,
  ILCatUmum,
  ILCatPsikiater,
  ILCatObat,
  ILCatAnak,
  ILHospitalBG,
};
