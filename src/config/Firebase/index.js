import firebase from 'firebase';

firebase.initializeApp({
  apiKey: 'AIzaSyB6Ff_eppiUIIiLvbt5iwEgLtwfsWkRvks',
  authDomain: 'my-doctor-d75bb.firebaseapp.com',
  projectId: 'my-doctor-d75bb',
  databaseURL:
    'my-doctor-d75bb-default-rtdb.asia-southeast1.firebasedatabase.app/',
  storageBucket: 'my-doctor-d75bb.appspot.com',
  messagingSenderId: '840013641442',
  appId: '1:840013641442:web:f34f1ad58968fff6b4d0bb',
  measurementId: 'G-8M4VNLGZJ9',
});
// firebase.analytics();

const Firebase = firebase;
export default Firebase;
